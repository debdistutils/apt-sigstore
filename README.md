# apt-sigstore

Apt-sigstore extends the [apt](https://salsa.debian.org/apt-team/apt)
package management tool with functionality related to
[Sigstore](https://sigstore.dev).

Apt is the main command-line package manager for [Trisquel
GNU/Linux](https://trisquel.info/), [PureOS](https://pureos.net/),
[Gnuinos](https://www.gnuinos.org/),
[Debian](https://www.debian.org/), [Ubuntu](https://ubuntu.com/),
[Devuan](https://www.devuan.org/) etc.

Apt-sigstore requires
[apt-verify](https://gitlab.com/debdistutils/apt-verify) and provides
two plugins:

- `apt-rekor` uses [rekor](https://docs.sigstore.dev/rekor/overview/)
  to confirm that the signature for the OpenPGP-signed apt archive
  index file `InRelease` have been recorded into the tamper-resistant
  public rekor log.  This offer transparency logging of the upstream
  distribution OpenPGP key, and makes targetted OpenPGP mis-use
  attacks more risky for an attacker since they need to make a global
  tamper-resistant public statement for every malicious signature they
  make using the distributions' OpenPGP key.  Still, auditing exactly
  what happened when an incident has happened is still difficult: the
  malicious `InRelease` file is not necessarily available anywhere.

- `apt-cosign` uses
  [cosign](https://docs.sigstore.dev/cosign/overview/) to verify that
  some third party has signed a confirmation of the same `InRelease`
  file using `cosign`.  The idea is for services to publish cosign
  signatures after they have seen and made some processing of the
  `InRelease` file.  Exactly what the guarantees would be is up to
  each such entity signing statements.  Our proof-of-concept
  [debdistcanary](https://gitlab.com/debdistutils/debdistcanary/)
  publisher merely claim that the `InRelease` file has been committed
  to a public GitLab git repository, to allow future auditing in case
  there is a key-compromise event.

Each plugin may be used independently of each other, but are both
implemented and documented in this project for coherence.

I believe the above two plugins, possibly with deployment of further
publishers of cosign-statements and a N-of-M criteria for
verification, would improve anyone's trust in the OpenPGP key usage of
well-known apt archives.

The idea is to further extend this functionality with per-package
statements such confirmation that a particular package has been
scheduled for a reproducible build, or a confirmation that a
particular package has been reproducibly built.  Compare
[reproduce-trisquel](https://gitlab.com/debdistutils/reproduce-trisquel).
This is work in progress, and may make sense to do at the `dpkg`-level
rather than `apt`-level, since the failure-mode of `apt` failures are
per-repository rather than per-package.

[[_TOC_]]

## Background

Apt-based distributions uses the `apt` or `apt-get` tools to install
and upgrade packages.  The process to make sure unintended packages
are not downloaded is based on an OpenPGP signature of the `InRelease`
file.  The signature is verified by [GnuPG](https://www.gnupg.org/)
using the
[gpgv](https://www.gnupg.org/documentation/manuals/gnupg/gpgv.html)
tool with a keyring of trusted OpenPGP keys stored in
`/etc/apt/trusted.gpg.d/`, which is normally installed by the
operating system when it was installed.

If someone uses this OpenPGP private key for malicious purposes they
can get your system to upgrade and install their packages, which could
even be used in a targetted attack that only affects certain users.

## Overview of apt-rekor

The purpose of `apt-rekor` is to make such attacks more risky for the
attacker, and to help understand what happened when such an attack
happens.  It does so by preventing use of a release file when the
signature of that file has not already been recorded into the rekor
ledger.

With this we achieve global public tamper-resistance transparency for
the release files that machines download and rely on.  This idea is
inspired by [Certificate
Transparency](https://en.wikipedia.org/wiki/Certificate_Transparency).

## Limitations of rekor

While `apt-rekor` provides a first step towards transparancy into apt
release index files its design has limitations.

The nature of [rekor](https://docs.sigstore.dev/rekor/overview/) is
such that it provides a globally tamper-resistant and publicly
auditable log that some particular data was digitally signed by a
particular public key.

However `rekor` does not record or store the signed data, nor does it
give any assurance as to the implications of the signed data.

Thus we still have the ability for an attacker to mis-use the OpenPGP
key of an apt archive, and publish malicious release index files and
send the signature of that malicious version to `rekor`.  Any system
that relies on `apt-rekor` will still install that file and trusts its
nefarious content.

You may ask what then is really the point of this, wasn't exactly that
what we wanted to protect against?  Then please observe that the
improvement `apt-rekor` provides is that the attacker is now in a
position they can't deny themselves out of: there is a globally
auditable tamper-resistance log with a checksum and signature of what
they created.  Assuming you trust `rekor` to provide property, of
course.

## Overview of apt-cosign

The next step to improve assurance is to publicly archive the release
index file, and sign that publication with a public key that can serve
as a trust anchor for public third-party publication of the release
index files.  To verify such publications, we use `apt-cosign`.

The [debdistget](https://gitlab.com/debdistutils/debdistget/) project
downloads `dist/` files from distributions, and
[debdistcanary](https://gitlab.com/debdistutils/debdistcanary/) is
responsible for 1) uploading the proper artifacts to the rekor log,
and 2) sign statements that they have seen a particular `InRelease`
file using cosign.

The `apt-rekor` script consumes the claims for verification from the
rekor ledger using to locally trusted OpenPGP-keys that apt trusts.

The `apt-cosign` script consumes the signatures created, and verify
them against a locally configured `/etc/apt/trusted.cosign.d/`
directory.

## Limitations of apt-cosign

The signature key used to sign the statements is key to the
protection, but unlike compromise of the main signing OpenPGP key of
your distribution, compromise of the private keys that `apt-cosign`
trusts only allows attackers to circumvent the additional protection
of `apt-cosign` but does not allow the attacker to install any
malicious software directly.  The idea is to make attacks more costly,
and to require the attacker to 1) steal the distribution OpenPGP
private key, 2) steal the cosign private key, and 3) publish publicly
logged tamper-resistant proofs of what they sign in the rekor ledger.
The hope is that this deters an attacker from chosing this attack
vector.

Further work in progress is to implement a N-out-of-M verification
approach, so that you can configure your system to require that N
hopefully independent entities have published cosign-statements before
you trust the `InRelease` file.

## Installation and dependencies

You need to make confirm that `apt` and GnuPG's `gpg` are available in
the path, typically `/usr/bin`.  The scripts works with any
POSIX-compliant /bin/sh such as dash or GNU bash, and uses `logger`
for sending messages to the system log file.  Other common tools like
`test`, `cat` and `sed` are required.

You need to install
[apt-verify](https://gitlab.com/debdistutils/apt-verify).

For example:

```
# run everything as root: su / sudo -i / doas -s
apt-get install -y apt gpg bsdutils wget
wget -nv -O/usr/local/bin/apt-verify-gpgv https://gitlab.com/debdistutils/apt-verify/-/raw/main/apt-verify-gpgv
chmod +x /usr/local/bin/apt-verify-gpgv
mkdir -p /etc/apt/verify.d
ln -s /usr/bin/gpgv /etc/apt/verify.d
echo 'APT::Key::gpgvcommand "apt-verify-gpgv";' > /etc/apt/apt.conf.d/75verify
```

You need [cosign](https://docs.sigstore.dev/cosign/installation/) and
[rekor-cli](https://docs.sigstore.dev/rekor/installation/) as well,
see below.

## Installation of apt-rekor

Copy the `apt-rekor` script into `/etc/apt/verify.d/`, and make sure
`rekor-cli' is installed.

```
# run everything as root: su / sudo -i / doas -s
wget -nv -O/usr/local/bin/rekor-cli 'https://github.com/sigstore/rekor/releases/download/v1.1.0/rekor-cli-linux-amd64'
echo afde22f01d9b6f091a7829a6f5d759d185dc0a8f3fd21de22c6ae9463352cf7d  /usr/local/bin/rekor-cli | sha256sum -c
chmod +x /usr/local/bin/rekor-cli
wget -nv -O/etc/apt/verify.d/apt-rekor https://gitlab.com/debdistutils/apt-sigstore/-/raw/main/apt-rekor
chmod +x /etc/apt/verify.d/apt-rekor
```

No further configuration is necessary!  Your system already has the
necessary OpenPGP trust anchors installed via apt.

## Installation of apt-cosign

Copy the `apt-cosign` script into `/etc/apt/verify.d/`, and make sure
`cosign` is installed.

For example:

```
# run everything as root: su / sudo -i / doas -s
wget -O/usr/local/bin/cosign https://github.com/sigstore/cosign/releases/download/v2.0.1/cosign-linux-amd64
echo 924754b2e62f25683e3e74f90aa5e166944a0f0cf75b4196ee76cb2f487dd980  /usr/local/bin/cosign | sha256sum -c
chmod +x /usr/local/bin/cosign
wget -nv -O/etc/apt/verify.d/apt-cosign https://gitlab.com/debdistutils/apt-sigstore/-/raw/main/apt-cosign
chmod +x /etc/apt/verify.d/apt-cosign
```

You need to trust a third party that publishes cosign signatures for
the `InRelease` file of your distribution.  Our sister project
[debdistcanary](https://gitlab.com/debdistutils/debdistcanary/) is a
proof-of-concept project that will cosign statements claiming that it
has seen a `InRelease` file that has been uploaded and committed into
a publicly archived GitLab repository.

Currently `debdistcanary` publishes statements for Trisquel GNU/Linux,
PureOS, Gnuinos, Ubuntu, Debian and Devuan.

```
mkdir -p /etc/apt/trusted.cosign.d
dist=$(lsb_release --short --id | tr A-Z a-z)
wget -O/etc/apt/trusted.cosign.d/cosign-public-key-$dist.txt "https://gitlab.com/debdistutils/debdistcanary/-/raw/main/cosign/cosign-public-key-$dist.txt"
echo "Cosign::Base-URL \"https://gitlab.com/debdistutils/canary/$dist/-/raw/main/cosign\";" > /etc/apt/apt.conf.d/77cosign
```

On a Trisquel GNU/Linux system, those commands will result in the
following files:

```
root@kaka:~# cat /etc/apt/apt.conf.d/77cosign
Cosign::Base-URL "https://gitlab.com/debdistutils/canary/trisquel/-/raw/main/cosign";
root@kaka:~# cat /etc/apt/trusted.cosign.d/cosign-public-key-trisquel.txt
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEJEln/PFy2gWXQEiI1Deb5DVzFikY
DtGlco8BkhJJCvbTX+AuuWtrcWmgE7bL33856udo/0+t5A665Y0YYawO4Q==
-----END PUBLIC KEY-----
root@kaka:~#
```

The public keys are in [the cosign/ sub-directory of the
debstcanary](https://gitlab.com/debdistutils/debdistcanary/-/tree/main/cosign)
project.

## Usage

Test everything by running `apt-get update`.  Let's walk through what
you may see in `/var/log/syslog` (repeated once for every apt source):

```
Apr 20 17:31:35 kaka apt-verify-gpgv: /etc/apt/verify.d/apt-cosign --homedir /tmp/apt-key-gpghome.jexI2A3yZ9 --keyring /tmp/apt-key-gpghome.jexI2A3yZ9/pubring.gpg --ignore-time-conflict --status-fd 3 /tmp/apt.sig.0fVHbr /tmp/apt.data.GCtanu
```

Above is `apt-verify` invoking `apt-cosign`, which starts and outputs the following:

```
Apr 20 17:31:35 kaka apt-cosign: signfile fca30385b31eeb672312a499557566c06cbce3f0a66282622b10e14ee8bbd349 /tmp/apt.sig.0fVHbr
Apr 20 17:31:35 kaka apt-cosign: datafile 2d6b1c251666dbd68cbcb1702b59c18a0e156846906387a976ca11c29953aa0d /tmp/apt.data.GCtanu
Apr 20 17:31:35 kaka apt-cosign: cosign verify-blob --key /etc/apt/trusted.cosign.d/cosign-public-key-trisquel.txt --signature https://gitlab.com/debdistutils/canary/trisquel/-/raw/main/cosign/cosign-2d6b1c251666dbd68cbcb1702b59c18a0e156846906387a976ca11c29953aa0d.txt /tmp/
apt.data.GCtanu
Apr 20 17:31:38 kaka apt-cosign-cli: Verified OK
```

That is `apt-cosign` deriving a URL from your configuration and the
SHA-256 hash of the `InRelease` file and passing that to `cosign
verify-blob`.  The URL contains the signature and as you can see,
verifiation was successful.

Let's continue, `apt-verify` invokes `apt-rekor` that outputs the following:

```
Apr 20 17:31:38 kaka apt-verify-gpgv: /etc/apt/verify.d/apt-rekor --homedir /tmp/apt-key-gpghome.jexI2A3yZ9 --keyring /tmp/apt-key-gpghome.jexI2A3yZ9/pubring.gpg --ignore-time-conflict --status-fd 3 /tmp/apt.sig.0fVHbr /tmp/apt.data.GCtanu
Apr 20 17:31:38 kaka apt-rekor: signfile fca30385b31eeb672312a499557566c06cbce3f0a66282622b10e14ee8bbd349 /tmp/apt.sig.0fVHbr
Apr 20 17:31:38 kaka apt-rekor: datafile 2d6b1c251666dbd68cbcb1702b59c18a0e156846906387a976ca11c29953aa0d /tmp/apt.data.GCtanu
Apr 20 17:31:38 kaka apt-rekor: signers 60364C9869F92450421F0C22B138CA450C05112F
Apr 20 17:31:38 kaka apt-rekor: keyfile f2806c709fa24c5930141eeb8f347b9b8b869497fc3a331f49af08e953f551ef /tmp/tmp.vtRftXBnFm
Apr 20 17:31:39 kaka apt-rekor-cli: Current Root Hash: c2d9aa26d9b10aca0729461f291952f128efd289aaeb923179120dfa5cb70db4
Apr 20 17:31:39 kaka apt-rekor-cli: Entry Hash: 24296fb24b8ad77a76724dff4140b9337aaaafe5cde2ce053bd7c6de98ee833d2d516643a588981f
Apr 20 17:31:39 kaka apt-rekor-cli: Entry Index: 18475580
Apr 20 17:31:39 kaka apt-rekor-cli: Current Tree Size: 14322422
Apr 20 17:31:39 kaka apt-rekor-cli: Checkpoint:
Apr 20 17:31:39 kaka apt-rekor-cli: rekor.sigstore.dev - 2605736670972794746
Apr 20 17:31:39 kaka apt-rekor-cli: 14322422
Apr 20 17:31:39 kaka apt-rekor-cli: wtmqJtmxCsoHKUYfKRlS8Sjv0omq65IxeRIN+ly3DbQ=
Apr 20 17:31:39 kaka apt-rekor-cli: Timestamp: 1682004693930446474
Apr 20 17:31:39 kaka apt-rekor-cli: 
Apr 20 17:31:39 kaka apt-rekor-cli: - rekor.sigstore.dev wNI9ajBGAiEAoNrujhQZvcon5Em0a0/t8lIlv4cxFty+CaPC6yGX5TQCIQC707Npe1WzlhY35+487caCaJBTi4EmwYHbOijvAul/sQ==
Apr 20 17:31:39 kaka apt-rekor-cli: 
Apr 20 17:31:39 kaka apt-rekor-cli: 
Apr 20 17:31:39 kaka apt-rekor-cli: Inclusion Proof:
Apr 20 17:31:39 kaka apt-rekor-cli: SHA256(0x01 | 24296fb24b8ad77a76724dff4140b9337aaaafe5cde2ce053bd7c6de98ee833d2d516643a588981f | 224e86f6d106d4aea4df8ff3269c748ff78d83c3ba4eb7ead2cc0ec07dd197d4) =
Apr 20 17:31:39 kaka apt-rekor-cli: #011278dca2fbdaa2f8cf60c86a5853ffa891a0604c7e770739ec1a1f64db6690e1d
Apr 20 17:31:39 kaka apt-rekor-cli: 
...
```

The output from `rekor-cli` is a bit noisy, we'll see if we can
summarize that somehow once we figure out what the critical content
is.

Finally:

```
Apr 20 17:31:39 kaka apt-verify-gpgv: /etc/apt/verify.d/gpgv --homedir /tmp/apt-key-gpghome.jexI2A3yZ9 --keyring /tmp/apt-key-gpghome.jexI2A3yZ9/pubring.gpg --ignore-time-conflict --status-fd 3 /tmp/apt.sig.0fVHbr /tmp/apt.data.GCtanu
```

That's the traditional GnuPG `gpgv` verification mechanism.  On
success, you won't see anything more.

A typical failure from `apt-rekor` is as follows:

```
Apr 15 15:12:07 kaka apt-rekor-cli: entry in log cannot be located
```

That means the log have no record of the `InRelease` file your system
downloaded.  You may be subject to a targetted attack, or (more
likely) the GitLab CI/CD jobs for
[debdistget](https://gitlab.com/debdistutils/debdistget/) or
[debdistcanary](https://gitlab.com/debdistutils/debdistcanary/)
haven't been invoked recently enough.

There may be other error messages from `apt-cosign` like this:

```
Apr 20 17:16:56 kaka apt-cosign-cli: Error: searching log query: [POST /api/v1/log/entries/retrieve][400] searchLogQueryBadRequest  &{Code:400 Message:invalid character 'O' looking for beginning of value}
Apr 20 17:16:56 kaka apt-cosign-cli: main.go:74: error during command execution: searching log query: [POST /api/v1/log/entries/retrieve][400] searchLogQueryBadRequest  &{Code:400 Message:invalid character 'O' looking for beginning of value}
```

Typically that means there was something wrong with downloading the
signature data.  Other errors may occur.  That means that the
publisher of cosign-statements hadn't properly processed the
`InRelease` file your system downloaded.  You may be subject to a
targetted attack, or (more likely) the GitLab CI/CD jobs for
[debdistget](https://gitlab.com/debdistutils/debdistget/) or
[debdistcanary](https://gitlab.com/debdistutils/debdistcanary/)
haven't been invoked recently enough.

On failures, the error messages you get from `apt-get update` are not
the most intuitive but look like this:

```
root@kaka:~# apt-get update
Hit:1 http://archive.trisquel.org/trisquel aramo InRelease
Hit:2 http://archive.trisquel.org/trisquel aramo-updates InRelease
Err:1 http://archive.trisquel.org/trisquel aramo InRelease
  At least one invalid signature was encountered.
Hit:3 http://archive.trisquel.org/trisquel aramo-security InRelease
Err:2 http://archive.trisquel.org/trisquel aramo-updates InRelease
  At least one invalid signature was encountered.
Err:3 http://archive.trisquel.org/trisquel aramo-security InRelease
  At least one invalid signature was encountered.
Reading package lists... Done
W: An error occurred during the signature verification. The repository is not updated and the previous index files will be used. GPG error: http://archive.trisquel.org/trisquel aramo InRelease: At least one invalid signature was encountered.
W: An error occurred during the signature verification. The repository is not updated and the previous index files will be used. GPG error: http://archive.trisquel.org/trisquel aramo-updates InRelease: At least one invalid signature was encountered.
W: An error occurred during the signature verification. The repository is not updated and the previous index files will be used. GPG error: http://archive.trisquel.org/trisquel aramo-security InRelease: At least one invalid signature was encountered.
W: Failed to fetch http://archive.trisquel.org/trisquel/dists/aramo/InRelease  At least one invalid signature was encountered.
W: Failed to fetch http://archive.trisquel.org/trisquel/dists/aramo-updates/InRelease  At least one invalid signature was encountered.
W: Failed to fetch http://archive.trisquel.org/trisquel/dists/aramo-security/InRelease  At least one invalid signature was encountered.
W: Some index files failed to download. They have been ignored, or old ones used instead.
root@kaka:~# 
```

Followed by a dump in syslog of the full release file data and
signature that was downloaded by apt.  The purpose of logging these
details is to have an audit log of what data a potential malicious
actor sent to your machine, for further examination.

```
Apr 13 15:23:38 kaka apt-rekor-datafile: Origin: Trisquel
Apr 13 15:23:38 kaka apt-rekor-datafile: Label: Trisquel
Apr 13 15:23:38 kaka apt-rekor-datafile: Suite: aramo-security
...
Apr 13 15:23:38 kaka apt-rekor-datafile:  fa0092666aed3cfa4db2c7c2f29bf78591c808b41a0db69b1281a8ad0602d403 3644849 main/Contents-armhf.gz
Apr 13 15:23:38 kaka apt-rekor-datafile:  8dd6c2a9145d60c965c9cebe2246270d4c41b73e5985c7195a377f0952254b9a 76361111 main/Contents-ppc64el
Apr 13 15:23:38 kaka apt-rekor-datafile:  002208c3766f5576beecdaa07ee01741c0ca01dc737f9cf39ad9e334c89512bb 4125476 main/Contents-ppc64el.gz
Apr 13 15:23:38 kaka apt-rekor-signfile: -----BEGIN PGP SIGNATURE-----
Apr 13 15:23:38 kaka apt-rekor-signfile: 
Apr 13 15:23:38 kaka apt-rekor-signfile: iQIzBAEBCgAdFiEEYDZMmGn5JFBCHwwisTjKRQwFES8FAmQ3S3gACgkQsTjKRQwF
Apr 13 15:23:38 kaka apt-rekor-signfile: ES+4xQ/+Nv2mJemgJ+12tq++SHyMRRoJATrr3kP6JO/Zna459VqEP0Y7YA49LTuA
...
Apr 13 15:23:38 kaka apt-rekor-signfile: VE7GWc7plX+0ciG6FhLLdiEybnklCjq/wbBh2eoRDn7ojZoLo+4=
Apr 13 15:23:38 kaka apt-rekor-signfile: =qqw8
Apr 13 15:23:38 kaka apt-rekor-signfile: -----END PGP SIGNATURE-----
```

## License

Any file in this project is free software: you can redistribute it
and/or modify it under the terms of the GNU Affero General Public
License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

See the file [COPYING](COPYING) or
[https://www.gnu.org/licenses/agpl-3.0.en.html] for license text.

## Status

This is work in progress.  Bugs may result in lack of package updates
on your machine, so be careful about missing security upgrades.

The [debdistcanary](https://gitlab.com/debdistutils/debdistcanary/)
project is continously uploading rekor and cosign statements for
Trisquel GNU/Linux, PureOS, Gnuinos, Ubuntu, Debian and Devuan to
`rekor` as of mid April 2023.

I'm using `apt-rekor` and `apt-cosign` on my laptop which is running
Trisquel GNU/Linux 11.0 aramo amd64 with many successful runs of
`apt-get update`.

I have tested installation of `apt-rekor` and at least one successful
run of `apt-get update` on Gnuinos amd64, Debian GNU/Linux bullseye on
amd64/ppc64el, Debian GNU/linux buster on amd64, Devuan, Ubuntu.  No
attempts yet for PureOS.

There should be no risk of installing any unintended updates since
this AUGMENT your current verification method (OpenPGP-based `gpgv`),
and does not REPLACE it.

You can temporarily `chmod -x /etc/apt/verify.d/apt-rekor` and `chmod
-x /etc/apt/verify.d/apt-cosign` and run `apt-get update` to have
everything be back to normal.

## Contact

Please test and [send a report](-/issues/) if something doesn't work.

The author of this project is [Simon
Josefsson](https://blog.josefsson.org/).
